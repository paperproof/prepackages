#! /bin/sh -e

# Copyright (c) 2014, Idha Limited.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the Idha Limited nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL IDHA LIMITED BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#Set initial variables:

CWD=$(pwd)
if [ "$TMP" = "" ]; then
  TMP=/tmp
fi

# The version which appears in the application's filename
VERSION=1.6.2

# If the version conflicts with the Slackware package standard
# The dash character ("-") is not allowed in the VERSION string
# You can set the PKG_VERSION to something else than VERSION
# PKG_VERSION=2002.2.1 # the version which appears in the package name. 

ARCH=${ARCH:-i486} # the architecture on which you want to build your package

# First digit is the build number, which specifies how many times it has buildeen builtclass="
# Second string is the short form of the authors name, typical three initials:w
BUILD=${BUILD:-1_idha}

# The application's name
APP=uuid

# The installation directory of the package (where its actual directory
# structure will be created) 
PKG=$TMP/package-$APP

if [ "$ARCH" = "i486" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
  LIBDIRSUFFIX=""
  SLKLDFLAGS=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
  SLKLDFLAGS="-L/usr/lib64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
  SLKLDFLAGS=""
fi

# Delete the leftover directories if they exist (due to a previous build)
# and (re)create the packaging directory
rm -rf $PKG 
mkdir -p $TMP $PKG
rm -rf $TMP/$APP-$VERSION

# Change to the TMP directory
cd $TMP || exit 1

# Extract the application source in TMP
# Note: if your application comes as a tar.bz2, you need tar -jxvf
tar -zxvf $CWD/$APP-$VERSION.tar.gz || exit 1

# Change to the application source directory
cd $APP-$VERSION || exit 1

# Change ownership and permissions if necessary
# This may not be needed in some source tarballs, but it never hurts
chown -R root:root .
chmod -R u+w,go+r-w,a-s .

# Set configure options
# If your app is written in C++, you'll also need to add a line for CXXFLAGS
LDFLAGS="$SLKLDFLAGS" \
CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
  ./configure \
            --prefix=/usr \
                --libdir=/usr/lib${LIBDIRSUFFIX} \
                --includedir=/usr/include/ossp \
      
# compile the source, but exit if anything goes wrong
make || exit

# Install everything into the package directory, but exit if anything goes wrong
make install DESTDIR=$PKG || exit

# Create a directory for documentation
mkdir -p $PKG/usr/doc/$APP-$VERSION

# Copy documentation to the docs directory and fix permissions
#cp -a BUGS Changes FAQ INSTALL LICENSE MANIFEST README TODO docs/ $PKG/usr/doc/$APP-$VERSION
#find $PKG/usr/doc/$APP-$VERSION -type f -exec chmod 644 {} \;

#cat $CWD/$APP.SlackBuild > $PKG/usr/doc/$APP-$VERSION/$APP.SlackBuild

# Create the ./install directory and copy the slack-desc into it
mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

# Add doinst.sh to package (if it exists)
if [ -e $CWD/doinst.sh.gz ]; then
  zcat $CWD/doinst.sh.gz > $PKG/install/doinst.sh
fi

# Strip some libraries and binaries
#( cd $PKG
#  find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
#  find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
#)

# Compress man pages if they exist
#if [ -d $PKG/usr/man ]; then
#  ( cd $PKG/usr/man
#    find . -type f -exec gzip -9 {} \;
#    for i in $(find . -type l) ; do ln -s $(readlink $i).gz $i.gz ; rm $i ; done
#  ) 
#fi

# Compress info pages if they exist (and remove the dir file)
#if [ -d $PKG/usr/info ]; then
#  gzip -9 $PKG/usr/info/*.info
#  rm -f $PKG/usr/info/dir
#fi

# Build the package
cd $PKG
/sbin/makepkg -l y -c n $TMP/$APP-$VERSION-$ARCH-$BUILD.tgz
